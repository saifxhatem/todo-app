<?php

use App\Http\Controllers\TodoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::controller(Todos::class)->group(function () {
//     Route::post('/todos/create', 'store');
//     Route::get('/todos/', 'index');
// });

Route::prefix('todos')->group(function () {
    Route::controller(TodoController::class)->group(function () {
        Route::post('/create', 'store');
        Route::get('/', 'index');
    });
});
